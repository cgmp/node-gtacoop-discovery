const dgram = require("dgram")
const protobuf = require("protobufjs")

const { EventEmitter } = require('events')

// lidgren.network message types
const NetMessageType = {
    Unconnected: 0,
    Discovery: 136,
    DiscoveryResponse: 137
}

// gtaserver packet types
const PacketType = {
    DiscoveryResponse: 7,
    PlayerListRequest: 1001
}

// load gtaserver protobuf templates
const root = protobuf.loadSync(__dirname + "/protocol.proto")
const ProtoMessage = {
    DiscoveryResponse: root.lookupType("DiscoveryResponse"),
    PlayerList: root.lookupType("PlayerList")
}

module.exports = class Client extends EventEmitter {
    /** @type string */
    address;
    /** @type number */
    port;
    /** @type dgram.Socket */
    client;

    /**
     * Constructs a new client connecting to address on port
     * @param {string} address
     * @param {number} port
     */
    constructor(address, port) {
        super()

        this.address = address
        this.port = port

        this.client = dgram.createSocket("udp4")
    }

    /**
     * Sends a discovery request to the server and awaits response
     * @returns {Promise}
     */
    discovery() {
        return new Promise((resolve, reject) => {
            this.client.once("message", (msg) => {
                const header = this.parseHeader(msg)

                if (header.messageType != NetMessageType.DiscoveryResponse) {
                    return reject(new Error(`Wrong message type, expected ${NetMessageType.DiscoveryResponse} got ${header.messageType}`))
                }

                const payload = msg.slice(5)

                // packet type - 32 bits
                // discovery response length - 32 bits
                // disocvery response
                const packetType = payload.readUInt32LE(0)
                const length = payload.readUInt32LE(4)

                if (packetType != PacketType.DiscoveryResponse) {
                    return reject(new Error(`Wrong packet type, expected ${PacketType.DiscoveryResponse} got ${packetType}`))
                }

                // decode protobuf payload
                const response = ProtoMessage.DiscoveryResponse.decode(msg.slice(13))

                resolve({ header: header, response: response })
            })
            this.client.send(Buffer.from([NetMessageType.Discovery, 0, 0, 0, 0]), this.port, this.address)

            setTimeout(() => { reject(new Error("Timed out")) }, 5 * 1000);
        })
    }

    players()
    {
        return new Promise((resolve, reject) => {
            this.client.once("message", (msg) => {
                const header = this.parseHeader(msg)

                if(header.messageType != NetMessageType.Unconnected) {
                    return reject(new Error(`Wrong message type, expected ${NetMessageType.Unconnected} got ${header.messageType}`))
                }

                const payload = msg.slice(5)
                const packetType = payload.readUInt32LE(0)
                const length = payload.readUInt32LE(4)

                if(packetType != PacketType.PlayerListRequest) {
                    return reject(new Error(`Wrong packet type, expected ${PacketType.PlayerListRequest} got ${packetType}`))
                }

                const response = ProtoMessage.PlayerList.decode(msg.slice(13));
                resolve({ header: header, response: response })
            })

            const header = Buffer.from([NetMessageType.Unconnected, 0, 0, 64, 0, 7])
            const command = Buffer.from("players");
            this.client.send(Buffer.concat([header, command]), this.port, this.address)

            setTimeout(() => { reject(new Error("Timed out")) }, 5 * 1000);
        })
    }

    /**
     * Closes the underlying udp client
     */
    close() {
        this.client.close()
    }

    /**
     * Returns a lidgren.network header from a buffer
     * @param {Buffer} buffer
     */
    parseHeader(buffer) {
        return {
            // https://github.com/lidgren/lidgren-network-gen3/blob/3ab2d8d36867ccd9e10d7b1c61ed4ccc8ce5172f/Lidgren.Network/NetPeer.Internal.cs#L502-L506
            messageType: buffer.readUInt8(0),
            sequenceNumber: buffer.readUInt16LE(1),
            payloadLength: (buffer[3] | (buffer[4] << 8)) / 8
        }
    }

    toString() {
        return `<Client ${this.address}:${this.port}>`
    }
}
