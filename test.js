const Client = require("./index.js")


const client = new Client("127.0.0.1", 4499);

// since either nodejs or my nodejs version doesn't support v8 top level await
(async () => {

const discovery = await client.discovery();
console.log(discovery)

const players = await client.players();
console.log(players)

})()
